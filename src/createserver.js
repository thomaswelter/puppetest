const fs = require('fs')
const http = require('http')
const mime = require('mime-types')
const path = require('path')

const createServer = base => {
	const requests = new Set
	const sockets = new Set
	const port = Math.floor(Math.random() * 2000) + 8000
	const url = 'http://127.0.0.1:' + port
	const server = http.createServer((req, res) => {
		const parsedUrl = new URL(req.url, url)
		const puppetestFile = path.join(__dirname, 'puppetest.js')
		const isPuppetest = parsedUrl.pathname.split('/').slice(-1)[0] == 'puppetest.js'
		const filePath = path.join(process.cwd(), parsedUrl.pathname)
		const servingFile = isPuppetest? puppetestFile : filePath
		requests.add(servingFile)

		fs.access(servingFile, fs.constants.R_OK, err => {
			if(err) {
				res.writeHead(400, {'Content-Type': 'text/plain'})
				res.end('ERROR: File does not exist')
				return
			}

			res.writeHead(200, {
				'Content-Type': mime.contentType(path.extname(servingFile)),
				'Access-Control-Allow-Origin': '*'
			})
			fs.createReadStream(servingFile).pipe(res)
		})
	})

	server.listen(port)
	server.on('connection', socket => {
		sockets.add(socket)
		socket.on('close', () => sockets.delete(socket))
	})

	const close = () => new Promise(resolve => {
		sockets.forEach(s => s.destroy())
		server.close(() => resolve([...requests]))
	})

	return {url, close}
}

module.exports = createServer