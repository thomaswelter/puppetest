const fs = require('fs')
const path = require('path')

const write = str => new Promise((resolve, reject) => {
	fs.writeFile(path.join(process.cwd(), 'puppetest-coverage.html'), str, err =>
		err? reject(err) : resolve())
})

const getTemplate = () => new Promise((resolve, reject) => {
	fs.readFile(path.join(__dirname, 'template.html'), (err, data) =>
		err? reject(err) : resolve(data))
})

const last = arr =>
	arr[arr.length -1]

const combineRanges = (a, b) => {
	const c = a.concat(b).sort((a, b) => a.start - b.start)
	let d = [], last
	for(let x of c) {
		if(!last || x.start > last.end)
			d.push(last = x)

		else if(x.end > last.end)
			last.end = x.end
	}
	return d
}

const groupBy = (xs, key) =>
	xs.reduce((a, c) => ({...a, [c[key]]: (a[c[key]] || []).concat(c)}), {})

const coveragePercentage = coverageData => {
	let total = 0, used = 0
	for(let entry of coverageData) {
		total += entry.text.length
		for(let range of entry.ranges)
			used += range.end - range.start -1
	}
	return Math.floor(used / total * 100)
}

const coverage = async data => {
	if(!data.length) return {percent: 0}

	const coverageMessages = data
		.flatMap(file => file.testMessages.flatMap(test =>
			test.messages.filter(m => m.type == 'coverage').flatMap(x => x.value)))
		.filter(x => /puppetest\.js$/.test(x.url) == false)

	const unduplicated = Object.values(groupBy(coverageMessages, 'text'))
		.map(group => group.reduce((a, c) => ({...a, ranges: combineRanges(a.ranges, c.ranges)})))

	const partials = unduplicated.map(item => {
		let html = '<pre>', tokens = item.text.split(''), r = item.ranges.slice()
		for(let i = 0; i < tokens.length; i++) {
			if(r[0] && r[0].start == i) html += '<span>'
			html += tokens[i]
			if(r[0] && r[0].end == i +1) {
				html += '</span>'
				r.shift()
			}
		}
		html += '</pre>'
		return {...item, html}
	})

	const templ = await getTemplate()

	const header = partials.reduce((a, c, i) => {
		const p = (new URL(c.url)).pathname
		return a + `<li><a href="#file-${i}">${(new URL(c.url)).pathname}</a></li>\n`
	}, '<div class="header"><ul class="center">\n') + '</ul></div>\n\n'

	const html = partials.reduce((a, c, i) => {
		const p = (new URL(c.url)).pathname
		return a + `<div class="center" id="file-${i}">\n\t<h1>${p}</h1>\n\t${c.html}\n</div>`
	}, templ + header)

	await write(html)

	return {percent: coveragePercentage(unduplicated)}
}

module.exports = coverage