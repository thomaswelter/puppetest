const createServer = require('./createserver.js')
const runTest = require('./runtest.js')
const {reportTestNum} = require('./log.js')

const findTests = async (file, browser) => {
	const start = Date.now()
	const msg = []
	const [server, page] = await Promise.all([
		createServer(file),
		browser.newPage()
	])
	const url = new URL(file, server.url)

	page.on('error', err => {throw err})
	page.on('pageerror', err => msg.push({type: 'error', value: err + ''}))
	page.on('console', log => msg.push({type: log.type(), value: log.text()}))

	await page.goto(url)

	const testkeys = await page.evaluate(() => Object.keys(window._tests || {}))
	const tests = testkeys.map(key => runTest(url, key, browser))
	reportTestNum(testkeys.length)

	const [testMessages] = await Promise.all([
		Promise.all(tests),
		page.close()
	])
	const requests = await server.close()

	return {file, testkeys, testMessages, pageMessages: msg, requests, time: Date.now() - start}
}

module.exports = findTests