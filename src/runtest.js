const {reportSucc, reportErr} = require('./log.js')

const runTest = async (url, testkey, browser) => {
	const msg = []
	const page = await browser.newPage()

	await page.coverage.startJSCoverage()

	page.on('error', err => {throw err})
	page.on('pageerror', err => msg.push({type: 'error', value: err + ''}))
	page.on('console', msg => msg.push({type: msg.type(), value: msg.text()}))

	await page.goto(url)
	
	const start = Date.now()
	msg.push(await page.evaluate(key => {
		return window._tests[key]? window._tests[key]() : {type: 'not found', key}
	}, testkey))
	const end = Date.now()

	const coverage = await page.coverage.stopJSCoverage()
	msg.push({type: 'coverage', value: coverage})
	await page.close()

	msg.forEach(m =>
		m.type == 'error'? reportErr(1) :
		m.type == 'success'? reportSucc(1) :
		undefined)

	return {messages: msg, time: end - start}
}

module.exports = runTest