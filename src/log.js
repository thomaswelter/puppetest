const chalk = require('chalk')

let testnum = 0,
	filenum = 0,
	succnum = 0,
	errnum = 0,
	id = null,
	spinnerIndex = 0,
	cache = {}

const isWatching = process.argv.includes('-w') || process.argv.includes('--watch')

const count = (data, type) =>
	data.flatMap(f => f.testMessages).flatMap(arr => arr.messages).filter(x => x.type == type).length

const log = (partial, coverage) => {
	partial.forEach(item => cache[item.file] = item)
	const data = Object.values(cache)

	clearInterval(id)
	const files = data.map(x => x.file)

	if(isWatching) console.log('\n'.repeat(3000))
	else process.stdout.clearLine(0)

	data.forEach(item => {
		console.log(chalk.bold.underline(`\n\t${item.file}:`))

		item.pageMessages.forEach(m => {
			if(m.type !== 'error') return
			console.log(chalk.bgRed(`\t\t${m.value.split('\n').join('\n\t\t')}\n`))
		})

		item.testMessages.forEach(t => {
			const time = t.time > 1000? chalk.grey(` (${t.time}ms)`) : ''
			t.messages.forEach(m => {
				if(m.type == 'error') {
					console.log(chalk.bgRed('\t[-] ' + m.name) + time)
					console.log(chalk.bgRed(`\t\t${m.value.split('\n').join('\n\t\t')}\n`))
				}

				else if(m.type == 'skipped')
					console.log(chalk.bgYellow('\t[ ] ' + m.name) + time)

				else if(m.type == 'success')
					console.log(chalk.green('\t[+] ') + m.name + time)
			})
		})
	})

	console.log(chalk.cyan(`\t- ${files.length} files (${coverage}% coverage)`))
	console.log(chalk.green(`\t- ${count(data, 'success')} success`))
	console.log(chalk.red(`\t- ${count(data, 'error')} error`))
	console.log(chalk.yellow(`\t- ${count(data, 'skipped')} skipped`))

	return count(data, 'error')? false : true
}

const spinnerToken = () =>
	(['-', '\\', '|', '/'])[(spinnerIndex++) % 4]

const reportTestNum = n =>
	testnum += n

const reportFileNum = n =>
	filenum += n

const reportSucc = n => {
	succnum += n
	testnum--
}

const reportErr = n => {
	errnum += n
	testnum--
}

const spinner = () => {
	if(isWatching) console.log('\n'.repeat(3000))
	testnum = filenum = succnum = errnum = 0
	clearInterval(id)
	id = setInterval(updateSpinner, 100)
}

const updateSpinner = () => {
	process.stdout.write([
		'\t',
		chalk.gray(spinnerToken()) + ' ',
		chalk.cyan(`${filenum} files `),
		chalk.yellow(`${testnum} pending `),
		chalk.green(`${succnum} success `),
		chalk.red(`${errnum} error `),
		'\r'
	].join(''))
}

module.exports = {log, reportFileNum, reportTestNum, reportSucc, reportErr, spinner}