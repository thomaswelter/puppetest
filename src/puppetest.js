window._tests = {}
let key = 0
let planValue = undefined
let asserted = 0

const test = (name, f) => {
	window._tests[key++] = () =>
		runTest(key, name, f)
}

test.cb = (name, f) => {
	window._tests[key++] = () =>
		runTestCallback(key, name, f)
}

test.skip = (name, f) => {
	window._tests[key++] = () =>
		({type: 'skipped', name, key})	
}

const runTest = (key, name, f) => new Promise(r => {
	const pass = value =>
		r({type: 'success', name, key, value})

	Promise.resolve()
		.then(_ => f({...methods, pass}))
		.then(_ => checkPlanned())
		.catch(e => r({type: 'error', name, key, value: '' + e.stack}))
		.then(_ => r({type: 'success', name, key}))
})

const runTestCallback = (key, name, f) => new Promise(r => {
	const end = message => {
		try {checkPlanned(); r({type: 'success', name, key, value: message})}
		catch(e) {r({type: 'error', name, key, value: '' + e.stack})}
	}
	const pass = message => {
		try {r({type: 'success', name, key, value: message})}
		catch(e) {r({type: 'error', name, key, value: '' + e.stack})}
	}
	try {f({...methods, end, pass})}
	catch(e) {r({type: 'error', name, key, value: '' + e.stack})}
})

const checkPlanned = () => {
	if(planValue == undefined && asserted < 1)
		throw new Error('No assertions have been made')

	if(planValue !== undefined && asserted !== planValue)
		throw new Error(`${planValue} were planned while ${asserted} have been called`)
}


// API

const methods = {
	plan: n => {
		planValue = n
	},

	pass: () => {
		throw new Error('you should not see this')
	},

	fail: reason => {
		asserted++
		throw new Error(reason || 'Test failed because t.fail() was called')
	},

	end: () => {
		throw new Error('t.end() can only be called on test.cb()')
	},

	is: (a, b, message) => {
		asserted++
		if(!Object.is(a, b))
			throw new Error(message || `Values are not the same: ${stringify(a)} /= ${stringify(b)}`)
	},

	not: (a, b, message) => {
		asserted++
		if(Object.is(a, b))
			throw new Error(message || `Values are the same ${stringify(a)} == ${stringify(b)}`)
	},

	truthy: (value, message) => {
		asserted++
		if(!value) throw new Error(`${value} is not truthy`)
	},

	falsey: (value, message) => {
		asserted++
		if(value) throw new Error(`${value} is not falsey`)
	},

	true: (value, message) => {
		asserted++
		if(value !== true) throw new Error(`${value} is not true`)
	},

	false: (value, message) => {
		asserted++
		if(value !== false) throw new Error(`${value} is not false`)
	},

	deepEqual: (a, b, message) => {
		asserted++

		if(!isDeepEqual(a, b))
			throw new Error(message || `Values are not deepEqual: ${stringify(a)} /= ${stringify(b)}`)
	},

	notDeepEqual: (a, b, message) => {
		asserted++

		if(isDeepEqual(a, b))
			throw new Error(message || `Values are deepEqual: ${stringify(a)} /= ${stringify(b)}`)
	},

	throws: (f, expected, message) => {
		asserted++
		try {f()}
		catch(e) {catchError(e, expected, message); return e}
		throw new Error(message || 'Function did not throw in t.throw()')
	},

	throwsAsync: (f, expected, message) => {
		return Promise.resolve()
			.then(_ => f())
			.then(_ => {
				throw new Error(message || 'Function did not throw in t.throw()')
			} , e => catchError(e, expected, message))
	},
}



// UTILS

const catchError = (e, expected, message) => {
	if(typeof expected == 'string' && e.message !== expected)
		throw new Error(`Function threw but with the wrong message:\nexpected: ${expected}\nactual: ${e.message}`)

	else if(typeof expected == 'function' && !(e instanceof expected))
		throw new Error(`Function threw but the error is the wrong instance.\n expected: ${expected}\nactual: ${e.constructor}`)

	else if(typeof expected == 'object') {
		if(expected.hasOwnProperty('instanceof') && !(e instanceof expected.instanceof))
			throw new Error(`Function threw but the error is the wrong instance.\n expected: ${expected}\nactual: ${e.constructor}`)

		if(expected.hasOwnProperty('is') && !Object.is(e, expected.is))
			throw new Error('Function threw but is not the same as specified')

		if(expected.hasOwnProperty('message') && e.message !== expected)
			throw new Error(`Function threw but with the wrong message:\nexpected: ${expected}\nactual: ${e.message}`)

		if(expected.hasOwnProperty('name') && e.name !== expected.name)
			throw new Error(`Function threw but has unexpexted name.\nexpected: ${expected.name}\nactual: ${e.name}`)

		if(expected.hasOwnProperty('code') && e.code !== expected.code)
			throw new Error(`Function threw but has unexpexted code.\nexpected: ${expected.code}\nactual: ${e.code}`)
	}
}

const stringify = o =>
	JSON.stringify(o, null, '\t')

const isPrimitive = x =>
	Object(x) !== x

const isDeepEqual = (a, b) => {
	if(isPrimitive(b) && isPrimitive(a))
		return a === b
	
	if(isPrimitive(a) || isPrimitive(b))
		return false

	for(let k in b)
		if(!isDeepEqual(a[k], b[k]))
			return false

	return true
}

export {test}