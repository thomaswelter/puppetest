#!/usr/bin/env node

const puppeteer = require('puppeteer')
const watch = require('node-watch')
const globCallback = require('glob')
const fs = require('fs')
const path = require('path')

const findTests = require('./findtests.js')
const {log, reportFileNum, spinner} = require('./log.js')
const coverage = require('./coverage.js')

const isWatching = process.argv.includes('-w') || process.argv.includes('--watch')

const writeDataLog = data => new Promise((resolve, reject) => {
	fs.writeFile(path.join(process.cwd(), 'puppetest-log.json'), JSON.stringify(data, null, '\t'), err =>
		err? reject(err) : resolve())
})

const runWatch = (file, test, browser) => {
	fs.access(file, fs.constants.R_OK, err => {
		if(err) return
		watch(file, {}, async (ev, name) => {
			if(ev !== 'update') return
			spinner()
			log([await findTests(test, browser)])
		})
	})
}

const glob = (path, options = {}) =>
	new Promise((resolve, reject) => globCallback(path, options, (err, files) => err? reject(err) : resolve(files)))

const init = async () => {
	spinner()
	const htmlfilePaths = await glob('**/*.test.html')
	if(!htmlfilePaths.length) {
		console.log('No test files found')
		process.exit(1)
	}
	
	reportFileNum(htmlfilePaths.length)
	const browser = await puppeteer.launch()

	const testFiles = htmlfilePaths.map(path => findTests(path, browser))
	const data = await Promise.all(testFiles)

	const {percent} = await coverage(data)
	const hasErrors = log(data, percent)

	await writeDataLog(data)
	
	if(isWatching) {
		data.forEach(item => {
			runWatch(item.file, item.file, browser)
			item.requests.forEach(r => runWatch(r, item.file, browser))
		})
	}

	else await browser.close()

	process.exit(hasErrors? 1 : 0)
}

init().catch(err => {
	console.log(err)
	process.exit(1)
})