# Puppetest
Test cliënt side javascript using the command line.

![alt text](cli-screenshot.jpg)

## How to use
This package is not available on npm so to use it you must:
* clone this repo
* run `npm link puppetest` inside the repo
* now you can use the `puppetest` command

1. Puppetest looks for *.test.html files inside the project directory
2. each test file can import "puppetest" (es6 import) and declare tests (see below)
3. all tests are run in isolation and in parallel
4. puppetest creates a puppetest-log.json file in the project root
5. puppetest creates a puppetest-coverage.html to check your project coverage per line

## CLI Options
run `puppetest -w` to enter watch mode. Puppetest then automaticly reruns tests when a test file or dependency is changed

## Example test file
```html
<!doctype html>

<!-- declare some html to use in your tests -->

<script type="module">

    // this dependency is automaticly resolved
    import { test } from './puppetest.js'
    
    // setup before each test
    var x = 1
    
    test('chech maths', t => {
        t.is(1 + x, 2)
    })
    
    test('async math', async t => {
        const y = await Promise.resolve(2)
        t.is(x + y, 3)
    })
    
    // no need to tear down, puppetest just closes the page
</script>
```

## API
- `t.plan(n)` set the expected number of assertions (defaults to one)
- `t.fail()`
- `t.pass()`
- `t.end()` use together with `test.cb(name, t => t.end())`
- `t.is(actual, expected)` compare using Object.is
- `t.not(actual, expected)` inverse of `t.is`
- `t.deepEqual(actual, expected)`
- `t.notDeepEqual(actual, expected)`
- `t.true(actual, expected)`
- `t.truthy(actual, expected)`
- `t.false(actual, expected)`
- `t.falsey(actual, expected)`
- `t.throws(fn)`